function renderPage(page) {
  // Очистка страницы
  document.body.innerHTML = "";

  // Создание header
  const header = document.createElement("header");
  const logoContainer = document.createElement("div");
  const logo = document.createElement("img");
  const logoText = document.createElement("span");
  const discoverLink = document.createElement("a");
  const joinLink = document.createElement("a");
  const signInLink = document.createElement("a");

  // Установка классов для стилей
  header.classList.add("header");
  logoContainer.classList.add("logo-container");
  logo.classList.add("logo");
  logoText.classList.add("logo-text");
  discoverLink.classList.add("link");
  joinLink.classList.add("link");
  signInLink.classList.add("link");

  // Установка атрибутов и текста
  logo.src = "./assets/icons/logo.svg";
  logoText.textContent = "Simo";
  discoverLink.textContent = "Discover";
  joinLink.textContent = "Join";
  signInLink.textContent = "Sign In";

  // Установка ссылок для роутинга
  discoverLink.addEventListener("click", () => renderPage("discover"));
  joinLink.addEventListener("click", () => renderPage("join"));
  signInLink.addEventListener("click", () => renderPage("render"));

  // Добавление элементов в header
  header.appendChild(logoContainer);
  logoContainer.appendChild(logo);
  logoContainer.appendChild(logoText);
  header.appendChild(discoverLink);
  header.appendChild(joinLink);
  header.appendChild(signInLink);

  // Создание hero секции
  const heroSection = document.createElement("section");
  heroSection.classList.add("hero-section");

  // Создание footer
  const footer = document.createElement("footer");
  const aboutLink = document.createElement("a");
  const contactLink = document.createElement("a");
  const crInfoLink = document.createElement("a");
  const twitterLink = document.createElement("a");
  const facebookLink = document.createElement("a");

  // Установка классов для стилей
  footer.classList.add("footer");
  aboutLink.classList.add("link");
  contactLink.classList.add("link");
  crInfoLink.classList.add("link");
  twitterLink.classList.add("link");
  facebookLink.classList.add("link");

  // Установка текста и ссылок для footer
  aboutLink.textContent = "About us";
  contactLink.textContent = "Contact";
  crInfoLink.textContent = "CR Info";
  twitterLink.textContent = "Twitter";
  facebookLink.textContent = "Facebook";

  // Добавление элементов в footer
  footer.appendChild(aboutLink);
  footer.appendChild(contactLink);
  footer.appendChild(crInfoLink);
  footer.appendChild(twitterLink);
  footer.appendChild(facebookLink);

  // Установка фона для разных страниц
  switch (page) {
    case "discover":
      header.style.backgroundColor = "#dbac74";
      heroSection.style.backgroundColor = "#dbac74";
      footer.style.backgroundColor = "#dbac74";
      heroSection.innerHTML = `
      <div class="discover-hero-section">
      <div class="discover-hero-text-content">
      <div class="discover-slogan">Discover new music</div>
      <div class="discover-buttons">
        <button class="purple-button">Charts</button>
        <button class="purple-button">Songs</button>
        <button class="purple-button">Artists</button>
      </div>
      <div class="discover-subtext">By joining, you can benefit by listening to the latest albums released.</div>
      </div>
      <div>
      <img src="./assets/images/music-titles.png" alt="Music Titles">
      </div>
    </div>
    
      `;
      header.removeChild(discoverLink);
      break;
    case "join":
      document.body.style.backgroundImage =
        'url("./assets/images/background-page-sign-up.png")';
      document.body.style.backgroundSize = "cover";
      document.body.style.backgroundRepeat = "no-repeat";
      document.body.style.backgroundPosition = "center";
      heroSection.innerHTML = `
      <div class="join-hero-section">
        <div class="join-form">
          <div class="form-row">
            <div class="lables">
              <label for="name">Name</label>
              <label for="password">Password</label>
              <label for="email">Email</label>
            </div>
            <div class="inputs">
            <input type="text" id="name" />
            <input type="password" id="password" />
            <input type="email" id="email" />
            </div>
          </div>
        </div>
        <button class="purple-button">Join in</button>
      </div>`;
      header.removeChild(joinLink);
      break;
    case "render":
    default:
      document.body.style.backgroundImage =
        'url("./assets/images/background-page-landing.png")';
      document.body.style.backgroundSize = "cover";
      document.body.style.backgroundRepeat = "no-repeat";
      document.body.style.backgroundPosition = "center";
      const renderHeroSection = document.createElement("div");
      renderHeroSection.classList.add("render-hero-section");
      // Создание элемента с лозунгом
      const slogan = document.createElement("div");
      slogan.classList.add("slogan");
      slogan.textContent = "Feel the music";
      // Создание элемента с подзаголовком
      const subtext = document.createElement("div");
      subtext.classList.add("subtext");
      subtext.textContent = "Stream over 10 million songs in one click";
      // Создание и роутинг кнопки
      const joinbutton = document.createElement("button");
      joinbutton.classList.add("purple-button");
      joinbutton.textContent = "Join now";
      joinbutton.addEventListener("click", () => renderPage("join"));
      // Добавление элементов внутрь родительского элемента
      renderHeroSection.appendChild(slogan);
      renderHeroSection.appendChild(subtext);
      renderHeroSection.appendChild(joinbutton);
      // Очистка содержимого heroSection
      while (heroSection.firstChild) {
        heroSection.removeChild(heroSection.firstChild);
      }
      // Добавление renderHeroSection внутрь heroSection
      heroSection.appendChild(renderHeroSection);
      break;
  }

  // Добавление элементов на страницу
  document.body.appendChild(header);
  document.body.appendChild(heroSection);
  document.body.appendChild(footer);
}

// Вызов функции для отображения изначальной страницы
renderPage("render");
