function createHeader() {
  // Создание элементов
  const header = document.createElement("header");
  const logoLink = document.createElement("a");
  const logo = document.createElement("img");
  const menu = document.createElement("img");
  const centerIconsContainer = document.createElement("div");
  const searchIconContainer = document.createElement("div");
  const boxIconContainer = document.createElement("div");
  const userIconContainer = document.createElement("div");
  const searchIcon = document.createElement("img");
  const boxIcon = document.createElement("img");
  const userIcon = document.createElement("img");
  const separator1 = document.createElement("div");
  const separator2 = document.createElement("div");

  // Установка классов для стилей
  header.classList.add("header");
  logoLink.classList.add("logo");
  logo.classList.add("logo");
  menu.classList.add("menu");
  centerIconsContainer.classList.add("center-icons-container", "desctop-only");
  searchIconContainer.classList.add("icon-container");
  boxIconContainer.classList.add("icon-container");
  userIconContainer.classList.add("icon-container");
  searchIcon.classList.add("icon");
  boxIcon.classList.add("icon");
  userIcon.classList.add("icon");
  separator1.classList.add("separator");
  separator2.classList.add("separator");

  // Установка атрибутов и путей к иконкам
  logo.src = "./assets/icons/logo.svg";
  menu.src = "./assets/icons/menu.svg";
  searchIcon.src = "./assets/icons/search.svg";
  boxIcon.src = "./assets/icons/box.svg";
  userIcon.src = "./assets/icons/user.svg";

  // Установка ссылки для логотипа
  logoLink.href = "#";

  // Добавление элементов в header
  header.appendChild(logoLink);
  logoLink.appendChild(logo);
  header.appendChild(centerIconsContainer);
  centerIconsContainer.appendChild(searchIconContainer);
  searchIconContainer.appendChild(searchIcon);
  centerIconsContainer.appendChild(separator1);
  centerIconsContainer.appendChild(boxIconContainer);
  boxIconContainer.appendChild(boxIcon);
  centerIconsContainer.appendChild(separator2);
  centerIconsContainer.appendChild(userIconContainer);
  userIconContainer.appendChild(userIcon);
  header.appendChild(menu);

  // Добавление header на страницу
  document.body.prepend(header);
}

// Вызов функции
createHeader();

function createHeroSection() {
  // Создание элементов
  const heroSection = document.createElement("section");
  const heroImage = document.createElement("img");
  const heroContent = document.createElement("div");
  const caption = document.createElement("h4");
  const title = document.createElement("h2");
  const priceContainer = document.createElement("div");
  const price1 = document.createElement("h2");
  const separator = document.createElement("div");
  const price2 = document.createElement("h3");
  const button = document.createElement("button");

  // Установка классов для стилей
  heroSection.classList.add("hero-section");
  heroImage.classList.add("hero-image");
  heroContent.classList.add("hero-content");
  caption.classList.add("caption");
  title.classList.add("title");
  priceContainer.classList.add("price-container");
  price1.classList.add("price");
  separator.classList.add("separator");
  price2.classList.add("price__line-through");
  button.classList.add("button");

  // Установка атрибутов и путей к изображениям
  heroImage.src = "./assets/images/hero.png";

  // Установка текстового содержимого
  caption.textContent = "hear it. feel it";
  title.innerHTML = "move with the<br>music";
  price1.textContent = "$435";
  price2.textContent = "$465";
  button.textContent = "buy now";

  // Добавление элементов в heroSection
  heroSection.appendChild(heroImage);
  heroSection.appendChild(heroContent);
  heroContent.appendChild(caption);
  heroContent.appendChild(title);
  heroContent.appendChild(priceContainer);
  priceContainer.appendChild(price1);
  priceContainer.appendChild(separator);
  priceContainer.appendChild(price2);
  heroContent.appendChild(button);

  // Добавление heroSection на страницу
  document.body.appendChild(heroSection);
}

// Вызов функции
createHeroSection();

function createColourSection() {
  // Создание элементов
  const colourSection = document.createElement("section");
  const title = document.createElement("h2");
  const carousel = document.createElement("div");
  const carouselWork = document.createElement("div");
  const image1 = document.createElement("img");
  const image2 = document.createElement("img");
  const image3 = document.createElement("img");
  const arrowLeft = document.createElement("i");
  const arrowRight = document.createElement("i");

  // Установка классов для стилей
  colourSection.classList.add("colour-section");
  title.classList.add("colour-title");
  carousel.classList.add("carousel");
  carouselWork.classList.add("carouselWork");
  image1.classList.add("image");
  image2.classList.add("image");
  image3.classList.add("image");
  arrowLeft.classList.add("arrow", "fas", "fa-chevron-left");
  arrowRight.classList.add("arrow", "fas", "fa-chevron-right");

  // Установка текстового содержимого
  title.innerHTML = "Our Latest<br>colour collection 2021";

  // Установка фонового изображения
  image1.src = "./assets/images/clour-01.png";
  image2.src = "./assets/images/clour-02.png";
  image3.src = "./assets/images/clour-03.png";

  // Добавление элементов в carousel

  carousel.appendChild(image2);
  carousel.appendChild(image1);
  carousel.appendChild(image3);

  carouselWork.appendChild(arrowLeft);
  carouselWork.appendChild(carousel);
  carouselWork.appendChild(arrowRight);

  // Добавление элементов в colourSection
  colourSection.appendChild(title);
  colourSection.appendChild(carouselWork);

  // Добавление colourSection на страницу
  document.body.appendChild(colourSection);
}

// Вызов функции
createColourSection();

function createFeatureSection() {
  // Создание элементов
  const section = document.createElement("section");
  const contentWrapper = document.createElement("div");
  const textColumn = document.createElement("div");
  const featureImage = document.createElement("img");

  // Установка классов для стилей
  section.classList.add("feature-section");
  contentWrapper.classList.add("content-wrapper");
  textColumn.classList.add("text-column");
  featureImage.classList.add("feature-image");

  // Установка текста и контента
  const featureText = "Good headphones</br>and loud music is all</br>you need";
  const featureData = [
    {
      icon: "./assets/icons/battery.svg",
      title: "Battery",
      description: "Battery 6.2V - AAC codec",
    },
    {
      icon: "./assets/icons/bluetooth.svg",
      title: "Bluetooth",
      description: "Bluetooth connectivity",
    },
    {
      icon: "./assets/icons/microphone.svg",
      title: "Microphone",
      description: "Built-in microphone",
    },
  ];

  // Создание текстового контента
  const featureTitle = document.createElement("h3");
  featureTitle.textContent = featureData[0].title;

  const featureDescription = document.createElement("h5");
  featureDescription.textContent = featureData[0].description;

  // Создание фич-областей
  const featureAreas = featureData.map((item) => {
    const featureArea = document.createElement("div");
    const graphic = document.createElement("div");
    const innergraphic = document.createElement("div");
    const icon = document.createElement("img");
    const info = document.createElement("div");
    const title = document.createElement("h3");
    const description = document.createElement("h5");
    const learnMoreLink = document.createElement("a");
    learnMoreLink.textContent = "Learn More";
    learnMoreLink.href = "#";

    featureArea.classList.add("feature-area");
    graphic.classList.add("graphic");
    innergraphic.classList.add("innergraphic");
    icon.src = item.icon;
    title.textContent = item.title;
    description.textContent = item.description;

    info.appendChild(title);
    info.appendChild(description);
    info.appendChild(learnMoreLink);
    featureArea.appendChild(graphic);
    graphic.appendChild(innergraphic);
    innergraphic.appendChild(icon);
    featureArea.appendChild(info);

    return featureArea;
  });

  // Добавление фич-областей в контейнер
  featureAreas.forEach((area) => {
    contentWrapper.appendChild(area);
  });

  // Установка пути к изображению
  featureImage.src = "./assets/images/feature.png";

  // Добавление элементов в секцию
  section.appendChild(contentWrapper);
  section.appendChild(featureImage);

  // Добавление секции на страницу
  document.body.appendChild(section);
}
// Вызов функции
createFeatureSection();

function createProductSection() {
  // Создание элементов
  const productSection = document.createElement("section");
  const title = document.createElement("h2");
  const subtitle = document.createElement("h3");
  const productWrapper = document.createElement("div");

  // Установка классов для стилей
  productSection.classList.add("product-section");
  title.classList.add("colour-title");
  subtitle.classList.add("subtitle");
  productWrapper.classList.add("product-wrapper");

  // Установка текстового содержимого
  title.textContent = "Our Latest Product";
  subtitle.innerHTML =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Maecenas facilisis nunc ipsum aliquam, ante.";

  // Добавление элементов в productSection
  productSection.appendChild(title);
  productSection.appendChild(subtitle);
  productSection.appendChild(productWrapper);

  // Создание графической части продукта
  createProductGraphic(
    "product-03.png",
    "#FFE5EE",
    productWrapper,
    "Red Headphone",
    "$256"
  );
  createProductGraphic(
    "product-02.png",
    "#E5F1FF",
    productWrapper,
    "Blue Headphone",
    "$235"
  );
  createProductGraphic(
    "product-01.png",
    "#E5FFFB",
    productWrapper,
    "Green Headphone",
    "$245"
  );

  // Добавление productSection на страницу
  document.body.appendChild(productSection);
}

// Функция для создания графической части продукта
function createProductGraphic(imagePath, color, container, title, price) {
  const productItem = document.createElement("div");
  const imageWrapper = document.createElement("div");
  const image = document.createElement("img");
  const circle = document.createElement("div");
  const icon = document.createElement("img");
  const info1 = document.createElement("div");
  const info2 = document.createElement("div");
  const stars = document.createElement("div");
  const rating = document.createElement("h6");
  const productTitle = document.createElement("h4");
  const productPrice = document.createElement("h5");

  stars.classList.add("stars");
  for (let i = 0; i < 5; i++) {
    const star = document.createElement("i");
    star.classList.add("fa", "fa-star");
    star.setAttribute("aria-hidden", "true");
    stars.appendChild(star);
  }

  // Установка классов для стилей
  productItem.classList.add("product-item");
  imageWrapper.classList.add("image-wrapper");
  circle.classList.add("circle");
  info1.classList.add("info1");
  info2.classList.add("info2");
  stars.classList.add("stars");

  // Установка стилей для цвета фона
  circle.style.backgroundColor = color;
  imageWrapper.style.backgroundColor = color;

  // Установка путей к изображениям
  image.src = `./assets/images/${imagePath}`;
  icon.src = "./assets/icons/shopping-cart.svg";

  // Установка текстового содержимого
  rating.textContent = "4,50";
  productTitle.textContent = title;
  productPrice.textContent = price;

  // Добавление элементов в productItem
  imageWrapper.appendChild(image);
  productItem.appendChild(imageWrapper);
  productItem.appendChild(circle);
  circle.appendChild(icon);
  productItem.appendChild(info1);
  productItem.appendChild(info2);
  info1.appendChild(stars);
  info1.appendChild(rating);
  info2.appendChild(productTitle);
  info2.appendChild(productPrice);

  // Добавление productItem в контейнер
  container.appendChild(productItem);
}

// Вызов функции
createProductSection();

function createBoxingSection() {
  // Создание элементов
  const section = document.createElement("section");
  const image = document.createElement("img");
  const contentWrapper = document.createElement("div");
  const textColumn = document.createElement("div");
  const title = document.createElement("h2");
  const list = document.createElement("ul");

  // Установка классов для стилей
  section.classList.add("boxing-section");
  contentWrapper.classList.add("box-content-wrapper");
  textColumn.classList.add("text-column");

  // Установка пути к изображению
  image.src = "./assets/images/boxing.png";

  // Установка текста и контента
  const listItems = [
    "5A Charger",
    "Extra battery",
    "Sophisticated bag",
    "User manual guide",
  ];

  title.innerHTML = "Whatever you get in the box";

  listItems.forEach((item) => {
    const listItem = document.createElement("li");
    const icon = document.createElement("img");
    const separatorHor = document.createElement("hr");
    separatorHor.classList.add("separatorHor");
    icon.src = "./assets/icons/arrow-in-circle.svg";
    listItem.innerHTML = `<span>${item}</span>`;
    listItem.prepend(icon);
    list.appendChild(listItem);
    list.appendChild(separatorHor);
  });

  // Добавление элементов в текстовый столбец
  textColumn.appendChild(title);
  textColumn.appendChild(list);

  // Добавление элементов в обертку
  contentWrapper.appendChild(textColumn);
  contentWrapper.appendChild(image);

  // Добавление элементов в секцию
  section.appendChild(contentWrapper);

  // Добавление секции на страницу
  document.body.appendChild(section);
}

// Вызов функции
createBoxingSection();

function createMainCtaSection() {
  // Создание элементов
  const section = document.createElement("section");
  const contentWrapper = document.createElement("div");
  const title = document.createElement("h1");
  const subtitle = document.createElement("h3");
  const formContainer = document.createElement("div");
  const emailInput = document.createElement("input");
  const submitButton = document.createElement("button");

  // Установка классов для стилей
  section.classList.add("main-cta-section");
  contentWrapper.classList.add("sub-content-wrapper");
  formContainer.classList.add("form-container");
  emailInput.setAttribute("type", "email");
  emailInput.setAttribute("placeholder", "Enter Your email address");

  // Установка текста
  title.textContent = "Subscribe";
  subtitle.textContent = "Lorem ipsum dolor sit amet, consectetur";
  submitButton.textContent = "Subscribe";

  // Добавление элементов в форму
  formContainer.appendChild(emailInput);
  formContainer.appendChild(submitButton);

  // Добавление элементов в обертку
  contentWrapper.appendChild(title);
  contentWrapper.appendChild(subtitle);
  contentWrapper.appendChild(formContainer);

  // Добавление обертки в секцию
  section.appendChild(contentWrapper);

  // Добавление секции на страницу
  document.body.appendChild(section);
}

// Вызов функции
createMainCtaSection();

function createFooter() {
  // Создание элементов
  const footer = document.createElement("footer");
  const logo = document.createElement("img");
  const socialIcons = document.createElement("div");
  const instagramIconContainer = document.createElement("div");
  const facebookContainer = document.createElement("div");
  const twitterIconContainer = document.createElement("div");
  const instagramIcon = document.createElement("img");
  const twitterIcon = document.createElement("img");
  const facebookIcon = document.createElement("img");
  const linksContainer = document.createElement("div");
  const homeLink = document.createElement("a");
  const aboutLink = document.createElement("a");
  const productLink = document.createElement("a");

  // Установка классов для стилей
  footer.classList.add("footer");
  logo.classList.add("footer-logo");
  socialIcons.classList.add("footer-social-icons");
  instagramIconContainer.classList.add("footer-icon-container");
  facebookContainer.classList.add("footer-icon-container");
  twitterIconContainer.classList.add("footer-icon-container");
  instagramIcon.classList.add("footer-icon");
  twitterIcon.classList.add("footer-icon");
  facebookIcon.classList.add("footer-icon");
  linksContainer.classList.add("footer-links-container", "desctop-only");

  // Установка атрибутов и путей к иконкам
  logo.src = "./assets/icons/logo.svg";
  instagramIcon.src = "./assets/icons/instagram.svg";
  twitterIcon.src = "./assets/icons/twitter.svg";
  facebookIcon.src = "./assets/icons/facebook.svg";

  // Установка ссылок для социальных иконок
  const instagramLink = document.createElement("a");
  instagramLink.href = "#";
  instagramLink.appendChild(instagramIcon);

  const twitterLink = document.createElement("a");
  twitterLink.href = "#";
  twitterLink.appendChild(twitterIcon);

  const facebookLink = document.createElement("a");
  facebookLink.href = "#";
  facebookLink.appendChild(facebookIcon);

  // Установка ссылок для центральных ссылок
  homeLink.href = "#";
  homeLink.textContent = "Home";

  aboutLink.href = "#";
  aboutLink.textContent = "About";

  productLink.href = "#";
  productLink.textContent = "Product";

  // Добавление элементов в footer
  footer.appendChild(logo);
  footer.appendChild(linksContainer);
  linksContainer.appendChild(homeLink);
  linksContainer.appendChild(aboutLink);
  linksContainer.appendChild(productLink);
  footer.appendChild(socialIcons);
  instagramIconContainer.appendChild(instagramLink);
  twitterIconContainer.appendChild(twitterLink);
  facebookContainer.appendChild(facebookLink);
  socialIcons.appendChild(instagramIconContainer);
  socialIcons.appendChild(twitterIconContainer);
  socialIcons.appendChild(facebookContainer);

  // Добавление footer на страницу
  document.body.appendChild(footer);
}

// Вызов функции для создания footer
createFooter();
